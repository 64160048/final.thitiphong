/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.thitiphong.finalproject_thitiphong;

/**
 *
 * @author kewwy
 */
public class Order {
    private String name;
    private String manu ;
    private int addtocart ;
    private String note;
    private String address;

    @Override
    public String toString() {
        return "Order{" + "name=" + name + ", manu=" + manu + ", addtocart=" + addtocart + ", note=" + note + ", address=" + address + '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManu() {
        return manu;
    }

    public void setManu(String manu) {
        this.manu = manu;
    }

    public int getAddtocart() {
        return addtocart;
    }

    public void setAddtocart(int addtocart) {
        this.addtocart = addtocart;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Order(String name, String manu, int addtocart, String note, String address) {
        this.name = name;
        this.manu = manu;
        this.addtocart = addtocart;
        this.note = note;
        this.address = address;
    }
}
